export default {
    USER_INPUT_VALIDATION : /^[a-zA-Z][a-zA0-9-_]{3,23}$/,
    PASSWORD_INOUT_VALIDATION : /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{8,24}$/

}