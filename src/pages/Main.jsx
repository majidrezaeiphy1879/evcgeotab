import React from "react";
import { GoogleMap, useJsApiLoader } from "@react-google-maps/api";
import Spinner from "react-bootstrap/Spinner";
import {
  Container,
  Col,
  Row,
  Button,
  Nav,
  Navbar,
  NavDropdown,
  Form,
  Image,
} from "react-bootstrap";

const cardiff = { lat: 51.481583, lng: -3.17909 };

function Main() {
  const { isLoaded } = useJsApiLoader({
    googleMapsApiKey: process.env.GOOGLE_MAP
  });
  return (
    <Container fluid>
      <Row style={{ marginTop: "8rem" }} className="p-4 main-page-style ">
        <Col>
          <span className="main-font ">
            <h3>EV Solution</h3>
            <p>
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi
              doloribus aliquam cumque accusamus magni quisquam, odit
              reprehenderit veritatis voluptate dicta officia repellendus?
              Recusandae soluta necessitatibus, aliquam commodi voluptatum nemo
              dignissimos! Tempore, ducimus provident! Fugit sit odio officia,
              necessitatibus explicabo provident debitis suscipit vel ullam
              asperiores at, sed molestias sint quos voluptas harum natus! Nihil
              minus provident labore unde ea, ad in, porro eaque harum eos
              repudiandae iste asperiores consectetur nostrum odio vel iure
              nobis dolore! Veniam, excepturi, esse hic tempore assumenda
              dignissimos debitis consequatur aperiam dolorum labore expedita
              reprehenderit nihil earum! Pariatur inventore dignissimos,
              deserunt incidunt tempora fugit deleniti consequatur at ab, iusto
              voluptas vero molestias sint debitis iste reiciendis doloribus a?
              Maxime, voluptas natus nisi recusandae eveniet deleniti rem earum
              repellat labore unde tenetur non est quae molestiae deserunt
              mollitia totam corrupti odit repudiandae fuga accusamus
              reprehenderit? Totam soluta qui voluptate nemo, et voluptas. Harum
              nam quod at incidunt! lorem1500
            </p>
          </span>
        </Col>
        <Col style={{ zIndex: "-1" }}>
          {!isLoaded ? (
            <div>
              <Spinner
                animation="border"
                role="status"
                className="d-flex align-items-center justify-content-center"
              >
                <span className="visually-hidden">Loading...</span>
              </Spinner>
            </div>
          ) : (
            <GoogleMap
              zoom={10}
              mapContainerStyle={{ height: "100%", width: "100%" }}
              center={cardiff}
            >
              {/* Additional Google Map components */}
            </GoogleMap>
          )}
        </Col>
      </Row>
    </Container>
  );
}

export default Main;
