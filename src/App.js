import './App.css';
import Navbarmenue from './components/Navbar';
import {Container, Col, Row, Button} from 'react-bootstrap'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Main from './pages/Main';
import Register from './pages/Register';
function App() {
  return (
    <BrowserRouter>
      <Navbarmenue />
      <Routes>
        <Route path='/' element={<Main/>}/>
        <Route path='/register' element={<Register/>}/>

      </Routes>
    </BrowserRouter>
  );
}

export default App;
