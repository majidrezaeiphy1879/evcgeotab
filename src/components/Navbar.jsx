import React from "react";
import {
  Container,
  Col,
  Row,
  Button,
  Nav,
  Navbar,
  NavDropdown,
  Form,
  Image,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import car from "../images/car.svg";
import {BiLogInCircle} from 'react-icons'
const linkitems = [
  { name: "Home", to: "/" },
  { name: "Services", to: "/services" },
  { name: "Login", to: "/login" , icon:'<BiLogInCircle/>'},
  { name: "Register", to: "/register" },
];
const navLink = linkitems.map((item, index) => (
  <Nav.Link href="#link">
   <Link to={item.to} className="nav-links button-33" key={index}>{item.name}</Link>
  </Nav.Link>
));
function Navbarmenue() {
  return (
    <Container fluid >
      <Row >
        <Col style={{position:'fixed'}}>
          <Navbar expand="lg" className="navbar-style">
            <Container fluid>
              <Navbar.Brand href="#">
                <Image src={car} rounded />
              </Navbar.Brand>
              <Navbar.Toggle aria-controls="navbarScroll" />
              <Navbar.Collapse id="navbarScroll">
                <Nav
                  className="me-auto my-2 my-lg-0"
                  style={{ maxHeight: "100px" }}
                  navbarScroll
                >  
                  {navLink}
                </Nav>
                <Form className="d-flex">
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"

                  />
                </Form>
              </Navbar.Collapse>
            </Container>
          </Navbar>
        </Col>
      </Row>
    </Container>
  );
}

export default Navbarmenue;
